import QtQuick 2.14
import QtQuick.Controls 2.14
import QtQuick.Layouts 1.15
import QtQuick.Window 2.14
import Control 1.0

Window{
  title:"Hanoi Tower"
  id:main
  width:1000
  height:500
  visible:true
  property int tower_from:0
  property int tower_to
  property int piece
  Control{
    id:control
  }
  Timer{
    id:timer1
    running:false
    interval:500
  }
  Timer{
    id:timer2
    running:false
    interval:1000
  }
  Timer{
    id:timer3
    running:false
    interval:1500
  }
  Timer{
    id:timer4
    running:false
    interval:2000
  }
  Timer{
    id:timer5
    running:false
    interval:2500
  }
  Timer{
    id:timer6
    running:false
    interval:3000
  }
  Timer{
    id:timer7
    running:false
    interval:3500
  }
  Timer{
    id:timer8
    running:false
    interval:4000
  }
  Timer{
    id:timer9
    running:false
    interval:4500
  }
  Timer{
    id:timer10
    running:false
    interval:5000
  }
  Timer{
    id:timer11
    running:false
    interval:5500
  }
  Timer{
    id:timer12
    running:false
    interval:6000
  }
  Timer{
    id:timer13
    running:false
    interval:6500
  }
  Timer{
    id:timer14
    running:false
    interval:7000
  }
  Timer{
    id:timer15
    running:false
    interval:7500
  }
  Timer{
    id:timer16
    running:false
    interval:8000
  }
  Timer{
    id:timer17
    running:false
    interval:8500
  }
  Timer{
    id:timer18
    running:false
    interval:9000
  }
  Timer{
    id:timer19
    running:false
    interval:9500
  }
  Timer{
    id:timer20
    running:false
    interval:10000
  }
  Timer{
    id:timer21
    running:false
    interval:10500
  }
  Timer{
    id:timer22
    running:false
    interval:11000
  }
  Timer{
    id:timer23
    running:false
    interval:11500
  }
  Timer{
    id:timer24
    running:false
    interval:12000
  }
  Timer{
    id:timer25
    running:false
    interval:12500
  }
  Timer{
    id:timer26
    running:false
    interval:13000
  }
  Timer{
    id:timer27
    running:false
    interval:13500
  }
  Timer{
    id:timer28
    running:false
    interval:14000
  }
  Timer{
    id:timer29
    running:false
    interval:14500
  }
  Timer{
    id:timer30
    running:false
    interval:15000
  }
  Timer{
    id:timer31
    running:false
    interval:15500
  }
  Timer{
    id:timer32
    running:false
    interval:16000
  }
  Timer{
    id:timer33
    running:false
    interval:16500
  }
  Timer{
    id:timer34
    running:false
    interval:17000
  }
  Timer{
    id:timer35
    running:false
    interval:17500
  }
  Timer{
    id:timer36
    running:false
    interval:18000
    onTriggered:{
      
    }
  }
  Timer{
    id:timer37
    running:false
    interval:18500
  }
  Timer{
    id:timer38
    running:false
    interval:19000
  }
  function tower1_to_tower2(){
    main.tower_from=0
    main.tower_to=1
  }
  function tower1_to_tower3(){
    main.tower_from=0
    main.tower_to=2
  }
  function tower2_to_tower1(){
    main.tower_from=1
    main.tower_to=0
  }
  function tower2_to_tower3(){
    main.tower_from=1
    main.tower_to=2
  }
  function tower3_to_tower1(){
    main.tower_from=2
    main.tower_to=0
  }
  function tower3_to_tower2(){
    main.tower_from=2
    main.tower_to=1
  }
  function switcher(){
    control.remove_piece(main.tower_from)
    main.piece=control.get_piece()
    var possible=false
    if(tower_row.children[main.tower_to].top_tower==-1){
      possible=true
      switch(main.tower_to){
        case 0:
          for(var numd=4;numd>=0;numd--){
            if(pieces1.children[numd].piece_width==main.piece){
              pieces1.children[numd].visible=true
              tower1.top_tower=numd
              tower1.start=numd
              control.insert_piece(main.tower_to,pieces1.children[numd].piece_width)
              break
            }
          }
          break
        case 1:
          for(var numd=4;numd>=0;numd--){
            if(pieces2.children[numd].piece_width==main.piece){
              pieces2.children[numd].visible=true
              tower2.top_tower=numd
              tower2.start=numd
              control.insert_piece(main.tower_to,pieces2.children[numd].piece_width)
              break
            }
          }
          break
        case 2:
          for(var numd=4;numd>=0;numd--){
            if(pieces3.children[numd].piece_width==main.piece){
              pieces3.children[numd].visible=true
              tower3.top_tower=numd
              tower3.start=numd
              control.insert_piece(main.tower_to,pieces3.children[numd].piece_width)
              break
            }
          }
      }
    }else{
      switch(main.tower_to){
        case 0:
          for(var numd=tower1.start-1;numd>=0;numd--){
            if(main.piece==pieces1.children[numd].piece_width&&tower1.top_tower>numd){
              possible=true
              tower1.top_tower=numd
              pieces1.children[numd].visible=true
              control.insert_piece(main.tower_to,pieces1.children[numd].piece_width)
              break
            }
          }
          break
        case 1:
          for(var numd=tower2.start-1;numd>=0;numd--){
            if(pieces2.children[numd].piece_width==main.piece&&tower2.top_tower>numd){
              possible=true
              tower2.top_tower=numd
              pieces2.children[numd].visible=true
              control.insert_piece(main.tower_to,pieces2.children[numd].piece_width)
              break
            }
          }
          break
        case 2:
          for(var numd=tower3.start-1;numd>=0;numd--){
            if(main.piece==pieces3.children[numd].piece_width&&tower3.top_tower>numd){
              possible=true
              tower3.top_tower=numd
              pieces3.children[numd].visible=true
              control.insert_piece(main.tower_to,pieces3.children[numd].piece_width)
              break
            }
          }
      }
    }
    if(possible){
      switch(main.tower_from){
        case 0:
          pieces1.children[tower1.top_tower].visible=false
          var top_before=-1
          for(var numd=tower1.top_tower+1;numd<5;numd++){
            if(pieces1.children[numd].visible==true){
              top_before=numd
              break
            }
          }
          tower1.top_tower=top_before
          if(top_before==-1)
            tower1.start=-1
            break
        case 1:
          pieces2.children[tower2.top_tower].visible=false
          var top_before=-1
          for(var numd=tower2.top_tower+1;numd<5;numd++){
            if(pieces2.children[numd].visible==true){
              top_before=numd
              break
            }
          }
          tower2.top_tower=top_before
          if(top_before==-1)
            tower2.start=-1
            break
        case 2:
          pieces3.children[tower3.top_tower].visible=false
          var top_before=-1
          for(var numd=tower3.top_tower+1;numd<5;numd++){
            if(pieces3.children[numd].visible==true){
              top_before=numd
              break
            }
          }
          tower3.top_tower=top_before
          if(top_before==-1)
            tower3.start=-1
      }
    }
  }
  Window{
    id:congratulations_window
    width:500
    height:500
    Column{
      leftPadding:congratulations_window.width/2
      topPadding:congratulations_window.height/2
      Text{
        text:"congratulations"
      }
    }
  }
  Column{
    Rectangle{
      id:menu_bar
      color:"dark gray"
      width:main.width
      height:50
      RowLayout{
        spacing:5
        Text{
          id:text_error
          visible:false
        }
        ComboBox{
          id:type_selector
          model:["automatic","manual"]
          Layout.leftMargin:5
          onActivated:{
            if(type_selector.currentIndex==1){
              if(tower1_selector.visible==false){
                tower1_selector.visible=true
                tower2_selector.visible=true
              }
            }else{
              if(tower1_selector.visible==true){
                tower1_selector.visible=false
                tower2_selector.visible=false
              }
            }
          }
        }
        Button{
          id:go
          text:"go"
          Layout.rightMargin:main.width/4
          onClicked:{
            if(type_selector.currentIndex==1){
              main.tower_from=tower1_selector.currentIndex
              main.tower_to=tower2_selector.currentIndex
              main.switcher()
              timer1.triggered.connect(function(){
                if(pieces3.children[0].visible==true&&pieces3.children[1].visible==true&&pieces3.children[2].visible==true&&pieces3.children[3].visible==true&&pieces3.children[4].visible==true){
                  congratulations_window.visible=true
                }
              })
              timer1.start()
            }else{
              main.tower1_to_tower2()
              main.switcher()
                
              timer1.triggered.connect(function(){
                main.tower1_to_tower3()
                main.switcher()
              })
              timer1.start()
              
              timer2.triggered.connect(function(){
                main.tower2_to_tower3()
                main.switcher()
              })
              timer2.start()
              
              timer3.triggered.connect(function(){
                main.tower1_to_tower2()
                main.switcher()
              })
              timer3.start()
              
              timer4.triggered.connect(function(){
                main.tower3_to_tower1()
                main.switcher()
              })
              timer4.start()
              
              timer5.triggered.connect(function(){
                main.tower3_to_tower2()
                main.switcher()
              })
              timer5.start()
              
              timer6.triggered.connect(function(){
                main.tower1_to_tower2()
                main.switcher()
              })
              timer6.start()
              
              timer7.triggered.connect(function(){
                main.tower1_to_tower3()
                main.switcher()
              })
              timer7.start()
              
              timer8.triggered.connect(function(){
                main.tower2_to_tower1()
                main.switcher()
              })
              timer8.start()
              
              timer9.triggered.connect(function(){
                main.tower2_to_tower3()
                main.switcher()
              })
              timer9.start()
              
              timer10.triggered.connect(function(){
                main.tower1_to_tower3()
                main.switcher()
              })
              timer10.start()
              
              timer11.triggered.connect(function(){
                main.tower2_to_tower1()
                main.switcher()
              })
              timer11.start()
              
              timer12.triggered.connect(function(){
                main.tower3_to_tower2()
                main.switcher()
              })
              timer12.start()
              
              timer13.triggered.connect(function(){
                main.tower3_to_tower1()
                main.switcher()
              })
              timer13.start()
              
              timer14.triggered.connect(function(){
                main.tower2_to_tower1()
                main.switcher()
              })
              timer14.start()
              
              timer15.triggered.connect(function(){
                main.tower3_to_tower2()
                main.switcher()
              })
              timer15.start()
              
              timer16.triggered.connect(function(){
                main.tower1_to_tower2()
                main.switcher()
              })
              timer16.start()
              
              timer17.triggered.connect(function(){
                main.tower1_to_tower3()
                main.switcher()
              })
              timer17.start()
              
              timer18.triggered.connect(function(){
                main.tower2_to_tower3()
                main.switcher()
              })
              timer18.start()
              
              timer19.triggered.connect(function(){
                main.tower1_to_tower2()
                main.switcher()
              })
              timer19.start()
              
              timer20.triggered.connect(function(){
                main.tower3_to_tower1()
                main.switcher()
              })
              timer20.start()
              
              timer21.triggered.connect(function(){
                main.tower3_to_tower2()
                main.switcher()
              })
              timer21.start()
              
              timer22.triggered.connect(function(){
                main.tower1_to_tower2()
                main.switcher()  
              })
              timer22.start()
              
              timer23.triggered.connect(function(){
                main.tower1_to_tower3()
                main.switcher()
              })
              timer23.start()
              
              timer24.triggered.connect(function(){
                main.tower2_to_tower1()
                main.switcher()
              })
              timer24.start()
              
              timer25.triggered.connect(function(){
                main.tower2_to_tower3()
                main.switcher()    
              })
              timer25.start()
              
              timer26.triggered.connect(function(){
                main.tower1_to_tower3()
                main.switcher()
              })
              timer26.start()
              
              timer27.triggered.connect(function(){
                main.tower2_to_tower1()
                main.switcher()
              })
              timer27.start()
              
              timer28.triggered.connect(function(){
                main.tower3_to_tower2()
                main.switcher()
              })
              timer28.start()
              
              timer29.triggered.connect(function(){
                main.tower3_to_tower1()
                main.switcher()
              })
              timer29.start()
              
              timer30.triggered.connect(function(){
                main.tower2_to_tower1()
                main.switcher()
              })
              timer30.start()
              
              timer31.triggered.connect(function(){
                main.tower2_to_tower3()
                main.switcher()
              })
              timer31.start()
              
              timer32.triggered.connect(function(){
                main.tower1_to_tower3()
                main.switcher()
              })
              timer32.start()
              
              timer33.triggered.connect(function(){
                main.tower1_to_tower2()
                main.switcher()  
              })
              timer33.start()
              
              timer34.triggered.connect(function(){
                main.tower3_to_tower2()
                main.switcher()
              })
              timer34.start()
              
              timer35.triggered.connect(function(){
                main.tower1_to_tower3()
                main.switcher()    
              })
              timer35.start()
              
              timer36.triggered.connect(function(){
                main.tower2_to_tower1()
                main.switcher()    
              })
              timer36.start()
              
              timer37.triggered.connect(function(){
                main.tower2_to_tower3()
                main.switcher()
              })
              timer37.start()
              
              timer38.triggered.connect(function(){
                main.tower1_to_tower3()
                main.switcher()
              })
              timer38.start()
            }
          }
        }
        ComboBox{
          id:tower1_selector
          model:[1,2,3]
          visible:false
        }
        ComboBox{
          id:tower2_selector
          model:[1,2,3]
          visible:false
        }
      }
    }
    Rectangle{
      id:towers 
      width:main.width
      height:main.height-menu_bar.width
      property color piece1_color:"blue"
      property color piece2_color:"red"
      property color piece3_color:"yellow"
      property color piece4_color:"orange"
      property color piece5_color:"green"
      property color tower_color:"light gray"
      color:"gray"
      Row{
        id:tower_row
        spacing:10
        topPadding:30
        leftPadding:30
        Rectangle{
          id:tower1
          property int top_tower:0
          property int start:4
          width:100
          height:200
          color:towers.tower_color
          Column{
            id:pieces1
            leftPadding:15
            topPadding:10
            Rectangle{
              color:towers.piece1_color
              width:40
              height:30
              property int piece_width:1
              visible:true
              anchors.horizontalCenter:parent.horizontalCenter
            }
            Rectangle{
              color:towers.piece2_color
              width:50
              height:30
              property int piece_width:2
              visible:true
              anchors.horizontalCenter:parent.horizontalCenter
            }
            Rectangle{
              color:towers.piece3_color
              width:60
              height:30
              property int piece_width:3
              visible:true
              anchors.horizontalCenter:parent.horizontalCenter
            }
            Rectangle{
              color:towers.piece4_color
              width:70
              height:30
              property int piece_width:4
              visible:true
              anchors.horizontalCenter:parent.horizontalCenter
            }
            Rectangle{
              color:towers.piece5_color
              width:80
              height:30
              property int piece_width:5
              visible:true
              anchors.horizontalCenter:parent.horizontalCenter
            }
          }
        }
        Rectangle{
          id:tower2
          property int top_tower:-1
          property int start:-1
          width:100
          height:200
          color:towers.tower_color
          Column{
            id:pieces2
            leftPadding:15
            topPadding:10
            Rectangle{
              color:towers.piece1_color
              width:40
              height:30
              property int piece_width:1
              visible:false
              anchors.horizontalCenter:parent.horizontalCenter
            }
            Rectangle{
              color:towers.piece2_color
              width:50
              height:30
              property int piece_width:2
              visible:false
              anchors.horizontalCenter:parent.horizontalCenter
            }
            Rectangle{
              color:towers.piece3_color
              width:60
              height:30
              property int piece_width:3
              visible:false
              anchors.horizontalCenter:parent.horizontalCenter
            }
            Rectangle{
              color:towers.piece4_color
              width:70
              height:30
              property int piece_width:4
              visible:false
              anchors.horizontalCenter:parent.horizontalCenter
            }
            Rectangle{
              color:towers.piece5_color
              width:80
              height:30
              property int piece_width:5
              visible:false
              anchors.horizontalCenter:parent.horizontalCenter
            }
          }
        }
        Rectangle{
          id:tower3
          property int top_tower:-1
          property int start:-1
          width:100
          height:200
          color:towers.tower_color
          Column{
            id:pieces3
            leftPadding:15
            topPadding:10
            Rectangle{
              color:towers.piece1_color
              width:40
              height:30
              property int piece_width:1
              visible:false
              anchors.horizontalCenter:parent.horizontalCenter
            }
            Rectangle{
              color:towers.piece2_color
              width:50
              height:30
              property int piece_width:2
              visible:false
              anchors.horizontalCenter:parent.horizontalCenter
            }
            Rectangle{
              color:towers.piece3_color
              width:60
              height:30
              property int piece_width:3
              visible:false
              anchors.horizontalCenter:parent.horizontalCenter
            }
            Rectangle{
              color:towers.piece4_color
              width:70
              height:30
              property int piece_width:4
              visible:false
              anchors.horizontalCenter:parent.horizontalCenter
            }
            Rectangle{
              color:towers.piece5_color
              width:80
              height:30
              property int piece_width:5
              visible:false
              anchors.horizontalCenter:parent.horizontalCenter
            }
          }
        }
      }
    }
  }
}
