MAX_STACK=5

stack_names_model=["top","pieces"]
stack_values_model=[-1,[int()]*MAX_STACK]
stack_model=dict(zip(stack_names_model,stack_values_model))

def insertion_stack(stack,piece):
  if empty(stack[0]) or piece<stack[1][stack[0]]:
    stack[0]+=1
    stack[1][stack[0]]=piece
  return stack

def remove_stack(stack):
  piece=stack[1][stack[0]]
  stack[1][stack[0]]=int()
  stack[0]-=1
  return stack,piece

full=lambda top:top==MAX_STACK-1
empty=lambda top:top==-1
