from stack import *
from PySide2.QtCore import QObject,Slot
from PySide2.QtQml import QQmlApplicationEngine,qmlRegisterType
from PySide2.QtGui import QGuiApplication

class Body(QObject):
  def __init__(self):
    super().__init__()
    self.__stacks=[stack_model]*3
    self.insert_piece(0,5)
    self.insert_piece(0,4)
    self.insert_piece(0,3)
    self.insert_piece(0,2)
    self.insert_piece(0,1)
    self.__piece=int()
  
  @Slot(result='QVariant')
  def get_piece(self):
    return self.__piece
  
  @Slot(int,int)
  def insert_piece(self,stack_target,piece):
    if not full(self.__stacks[stack_target]["top"]):
      values=stack_values_model
      values[0]=self.__stacks[stack_target]["top"]
      values[1]=self.__stacks[stack_target]["pieces"].copy()
      values=insertion_stack(values,piece)
      self.__stacks[stack_target]=dict(zip(stack_names_model,values))
  
  @Slot(int)
  def remove_piece(self,stack_target):
    if not empty(self.__stacks[stack_target]["top"]):
      values=stack_values_model
      values[0]=self.__stacks[stack_target]["top"]
      values[1]=self.__stacks[stack_target]["pieces"].copy()
      values,self.__piece=remove_stack(values)
      self.__stacks[stack_target]=dict(zip(stack_names_model,values))
    
if __name__=="__main__":
  app=QGuiApplication()
  engine=QQmlApplicationEngine()
  #engine.rootContext().setContextProperty("control",body)
  qmlRegisterType(Body,"Control",1,0,"Control")
  engine.load("ui.qml")
  app.exec_()
